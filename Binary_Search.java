import java.util.Scanner;
import java.util.Arrays;
public class Binary_Search
{
    public static void main(String args[])
    {
        Scanner input=new Scanner(System.in);
        int arr[]=new int[100];
        int num,i,n,beg,end,mid,found=0;
        System.out.println("\nEnter the Number of Elements in the Array : ");
        n=input.nextInt();
        System.out.println("\nEnter the Elements in the Array : ");
        for(i=0;i<n;i++)
        {
            arr[i]=input.nextInt();
        }
        Arrays.sort(arr,0,n);
        System.out.println("\nThe Sorted Array is : ");
        for(i=0;i<n;i++)
        {
            System.out.println(arr[i]);
        }
        System.out.println("\nEnter the element that has to be Searched : ");
        num=input.nextInt();
        beg=0;
        end=n-1;
        while(beg<=end)
        {
            mid=(beg+end)/2;
            if(arr[mid]==num)
            {
                found=1;
                System.out.println(num+" is Present in the Array at Position = "+(mid+1));
                break;
            }
            else if(arr[mid]>num)
            {
                end=mid-1;
            }
            else
            {
                beg=mid+1;
            }
        }
        if(beg>end && found==0)
        {
            System.out.println(num+" does not Exist in the Array");
        }
    }
}