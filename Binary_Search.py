arr=[]
found=0
n=int(input("Enter the Number of Elements in the Array : "))
print("Enter the Elements in the Array : ")
for i in range(0,n):
    arr.append(int(input()))
print("The Sorted Array is : ")
arr.sort()
print(arr)
num=int(input("Enter the Number to be Searched : "))
beg=0
end=n-1
while beg<=end:
    mid=int((beg+end)/2)
    if arr[mid]==num:
        found=1
        print("{} is present in the Array at {}".format(num,mid+1))
        break
    elif arr[mid]>num:
        end=mid-1
    else:
        beg=mid+1
if beg>end and found==0:
    print("{} is not Found in the Array".format(num))