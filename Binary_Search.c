#include<stdio.h>
void Sort(int arr[],int n);
int main()
{
    int arr[100], num, i, n, beg, end, mid, found=0;
    printf("\nEnter the number of elements in the array: \n");
    scanf("%d", &n);
    printf("\nEnter the elements: \n");
    for(i=0;i<n;i++)
    {
        scanf("%d", &arr[i]);
    }
    Sort(arr,n);
    printf("The Sorted Array is :\n");
    for(i=0;i<n;i++)
    {
        printf("%d  ",arr[i]);
    }
    printf("\nEnter the Number that has to be Searched : ");
    scanf("%d",&num);
    beg=0;
    end=n-1;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(arr[mid]==num)
        {
            found=1;
            printf("\n%d is Present in the Array at Position = %d",num,mid+1);
            break;
        }
        else if(arr[mid]>num)
        {
            end=mid-1;
        }
        else
        {
            beg=mid+1;
        }
    }
    if(beg>end && found==0)
    {
        printf("%d does not Exist in the Array",num);
    }
    return 0;
}
void Sort(int arr[],int n)
{
    int i,j,temp;
    for(i=0;i<n-1;i++)
    {
        for(j=0;j<n-i-1;j++)
        {
            if(arr[j]>arr[j+1])
            {
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }
}
